﻿
#TRABAJO PRACTICO Nº4 

API REST en Spring

Goals 

Mvn clean: El plugin Clean de Maven intenta limpiar los archivos y directorios generados por Maven durante su compilación. Si bien hay complementos que generan archivos adicionales, este plugin asume que estos archivos se generan dentro del directorio de destino.

Mvn compile: Este comando de Maven sirve para compilar el código fuente del proyecto

Mvn package: toma el código compilado y lo empaqueta o en su formato distribuible, como un JAR (Java Archive)

Mvn install: Apache Maven tiene una estrategia de dos niveles para resolver y distribuir archivos, que son llamados artefactos, con este comando (mvn install), se pueden colocar los artefactos en el repositorio local

Scopes (alcance)

Compile

Este es el scope predeterminado, utilizado si no se especifica ninguno. Las dependencias de compilación están disponibles en todos los classpaths de un proyecto. Además, esas dependencias se propagan a proyectos dependientes.

Provided

Este scope es muy similar a la compilación, pero indica que espera que el JDK o un contenedor proporcionen la dependencia en tiempo de ejecución. Este alcance solo está disponible en el classpath de compilación y prueba, y no es transitivo.

Runtime

Este ámbito indica que la dependencia no es necesaria para la compilación, pero si para la ejecución. Está en los classpaths de tiempo de ejecución y de prueba, pero no en el classpath de compilación.

Test

Este ámbito indica que la dependencia no es necesaria para el uso normal de la aplicación, y solo está disponible para las fases de compilación y ejecución de pruebas. Este alcance no es transitivo.

System

Este ámbito es similar al provider, excepto que debe proporcionar el JAR que lo contiene explícitamente. El artefacto siempre está disponible y no se busca en un repositorio.

Import (solo disponible en Maven 2.0.9 o posterior)

Este alcance solo se admite en una dependencia de tipo pom en la sección <dependencyManagement>. Indica la dependencia que se reemplazará con la lista efectiva de dependencias en la sección <dependencyManagement> del POM especificado. Dado que son reemplazados, las dependencias con un alcance de importación en realidad no participan en la limitación de la transitividad de una dependencia.

Arquetype

Un arquetype es un patrón o modelo original sobre el que pueden desarrollar todas aquellas cosas que son de un mismo tipo. Puede decirse que son plantillas, parametrizadas o configuradas para utilizar determinadas tecnologías, que los programadores utilizan como base para escribir y organizar el código de la aplicación. Es la estructura básica del proyecto.

Estructura básica de un proyecto Maven:

•	src/main/java: donde guardaremos nuestras clases java fuente. Debajo de esta carpeta situaremos nuestras clases en distintos paquetes.

•	src/main/resources: aquí almacenaremos los recursos (ficheros xml, ficheros de propiedades, imagenes) que pueda necesitar las clases java de nuestro proyecto.Igualmente aquí tienen que ir los ficheros de configuración de Spring o Hibernate por ejemplo.

•	src/test/java: en dicha carpeta se guardan las clases de test que se encargarán de probar el correcto funcionamiento de nuestra aplicación.

Arquetype y Artifact

Un artifact maven es un recurso generado por un proyecto de maven. Cada proyecto maven puede tener exactamente un artifact como un jar, war, ear, etc. El archivo de configuración del proyecto "pom.xml" describe cómo se construye el artifact, cómo se ejecutan las pruebas unitarias, etc.
La diferencia entre arquetype y artifact es que el artifact es parte del arquetype del proyecto Maven, ya que, como se explicó anteriormente, un arquetype es la estructura básica del proyecto mientas que artifac es in recurso propio de ese proyecto

Stereotypes Spring

Component: Es el estereotipo general y permite anotar un bean para que Spring lo considere uno de sus objetos.

Repository: Es el estereotipo que se encarga de dar de alta un bean para que implemente el patrón repositorio que es el encargado de almacenar datos en una base de datos o repositorio de información que se necesite. Al marcar el bean con esta anotación Spring aporta servicios transversales como conversión de tipos de excepciones.

Service: Este estereotipo se encarga de gestionar las operaciones de negocio más importantes a nivel de la aplicación y aglutina llamadas a varios repositorios de forma simultánea. Su tarea fundamental es la de agregar.

Controller: El último de los estereotipos que es el que realiza las tareas de controlador y gestión de la comunicación entre el usuario y el aplicativo. Para ello se apoya habitualmente en algún motor de plantillas o librería de etiquetas que facilitan la creación de páginas.


Verbos HTTP

Los verbos HTTP le indican al servidor qué hacer con los datos identificados por la URL.

GET

El método GET solicita una representación de un recurso específico. Las peticiones que usan el método GET sólo deben recuperar datos.

HEAD

El método HEAD pide una respuesta idéntica a la de una petición GET, pero sin el cuerpo de la respuesta.

POST

El método POST se utiliza para enviar una entidad a un recurso en específico, causando a menudo un cambio en el estado o efectos secundarios en el servidor.

PUT

El modo PUT reemplaza todas las representaciones actuales del recurso de destino con la carga útil de la petición.

DELETE

El método DELETE borra un recurso en específico.

CONNECT

El método CONNECT establece un túnel hacia el servidor identificado por el recurso.

OPTIONS

El método OPTIONS es utilizado para describir las opciones de comunicación para el recurso de destino.

TRACE

El método TRACE  realiza una prueba de bucle de retorno de mensaje a lo largo de la ruta al recurso de destino.

PATCH

El método PATCH  es utilizado para aplicar modificaciones parciales a un recurso.

