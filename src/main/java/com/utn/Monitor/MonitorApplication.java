// Decompiled using: fernflower
// Took: 57ms

package com.utn.Monitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonitorApplication {
    public MonitorApplication() {
        super();
    }
    
    public static void main(String[] args) {
        SpringApplication.run((Class)MonitorApplication.class, args);
    }
}
