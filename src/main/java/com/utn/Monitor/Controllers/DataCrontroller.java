// Decompiled using: fernflower
// Took: 21ms

package com.utn.Monitor.Controllers;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import com.utn.Monitor.Models.Data;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.utn.Monitor.Services.DataService;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DataCrontroller {
    @Autowired
    private DataService dataService;
    
    public DataCrontroller() {
        super();
    }
    
    @RequestMapping(value = { "/data" }, method = { RequestMethod.GET })
    public List<Data> dataGet() {
        return (List<Data>)this.dataService.getData();
    }
    
    @RequestMapping(value = { "/data" }, method = { RequestMethod.POST })
    public Data dataPost(@RequestBody Data data) {
        return this.dataService.save(data);
    }
}
