// Decompiled using: fernflower
// Took: 11ms

package com.utn.Monitor.Models;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Entity;

@Entity
public class Data {
    @Id
    @GeneratedValue
    private long id;
    private String browser;
    private String so;
    
    public Data() {
        super();
    }
    
    public Data(String browser, String so) {
        super();
        this.browser = browser;
        this.so = so;
    }
    
    public void setBrowser(String browser) {
        this.browser = browser;
    }
    
    public void setSo(String so) {
        this.so = so;
    }
    
    public String getBrowser() {
        return this.browser;
    }
    
    public String getSo() {
        return this.so;
    }
}
