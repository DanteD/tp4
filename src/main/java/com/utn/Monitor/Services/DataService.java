// Decompiled using: fernflower
// Took: 7ms

package com.utn.Monitor.Services;

import com.utn.Monitor.Models.Data;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.utn.Monitor.Repositories.DataRepository;
import org.springframework.stereotype.Service;

@Service
public class DataService {
    @Autowired
    private DataRepository dataRepository;
    
    public DataService() {
        super();
    }
    
    public List<Data> getData() {
        return (List<Data>)this.dataRepository.findAll();
    }
    
    public Data save(Data data) {
        return this.dataRepository.save(data);
    }
}
