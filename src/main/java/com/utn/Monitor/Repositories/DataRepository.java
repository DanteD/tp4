// Decompiled using: fernflower
// Took: 16ms

package com.utn.Monitor.Repositories;

import org.springframework.stereotype.Repository;
import com.utn.Monitor.Models.Data;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface DataRepository extends JpaRepository<Data, Long>
{
}
